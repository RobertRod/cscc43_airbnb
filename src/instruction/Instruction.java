package instruction;

/*
 * This is the class of hard-coded instruction that might be used
 * in different conditions to guide users or report invalid operations
 */
public class Instruction {
	private static Instruction instance = null;
	
	private Instruction() {}
	
	public static Instruction init() {
		if (instance == null) {
			instance = new Instruction();
		}
		return instance;
	}
	
	/*
	 * Instructions to give feedback to users
	 */
	public String welcome() {
		return "Hello, welcome to airbnb console\n";
	}	
	
	public String manual() {
		String commandGuide = "Below are a list of commonly used command that navigate through our program: \n"
				+ "+---------------------------------------------------------------+\n"
				+ "|                            General                            |\n"
				+ "+---------------------------------------------------------------+\n"
				+ "| - \"new host\": create a new host account\n"
				+ "| - \"new renter\": create a new renter account\n"
				+ "| - \"delete user\": delete a user and all its relevant info\n"
				+ "| - \"show owning\": get all listings under a host\n"
				+ "| - \"show renters\": get all renters with filter options\n"
				+ "| - \"exit\": log out from your account and exit the program\n"
				+ "+---------------------------------------------------------------+\n"
				+ "|                           As a host                           |\n"
				+ "+---------------------------------------------------------------+\n"
				+ "| - \"new listing\": upload a new listing\n"
				+ "| - \"new idle\": set available date for your listing\n"
				+ "| - \"delete idle\": remove available date for your listing\n"
				+ "| - \"update idle\": update price for your listing\n"
				+ "| - \"delete listing\": delete listing an all its relevant info\n"
				+ "| - \"new comment by host\": give comment to a renter\n"
				+ "+---------------------------------------------------------------+\n"
				+ "|                           As a renter                         |\n"
				+ "+---------------------------------------------------------------+\n"
				+ "| - \"book listing\": book a specific listing\n"
				+ "| - \"cancel booking\": cancel a booked listing\n"
				+ "| - \"new comment by renter\": give comment to a host as a renter\n";
		return commandGuide;
	}
	
	public String goodbye() {
		String endStatement = "Thanks for using airbnb console, hope to see you again.\n";
		return endStatement;
	}
	
	public String validStep() {
		return "-- Operation executed --\n";
	}
	
	
	
	/*
	 * Instructions to report error / potential incomplete operation to users
	 */
	public String incompleteQuery() {
		String report = "To execute the above task, some extra information are required:";
		return report;
	}
	
	public String consoleCrash() {
		String report = "The console crashed due to unexpected factor. Your data has been saved safely. Please try to run the program again.\n";
		return report;
	}
	
	public String invalidSyntax() {
		String report = "The syntax of your input cannot be recognized, please check the user manual.\n";
		return report;
	}
	
	public String conflictOperation() {
		String report = "The information you are trying to retrieve/modify is either provided in the wrong format, or violate the contract.\n";
		return report;
	}
	
	/*
	 * Instruction to give relevant feedback to us
	 */
	public String databaseInitFailure() {
		return "Program initialized failure\n";
	}
	
}
