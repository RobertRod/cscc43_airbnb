package instruction;

import java.util.HashMap;
import java.util.Scanner;

import airbnb.Console;
import query.*;

public class PartialQueryFields {
	private HashMap<String, requisionData> fieldMap = new HashMap<String, requisionData>();

	public PartialQueryFields() {
		fieldMap.put(CreateCommentToRenter.mySyntax, new requisionData(new CreateCommentToRenter()));
		fieldMap.put(CreateCommentToListing.mySyntax, new requisionData(new CreateCommentToListing()));
		fieldMap.put(CreateHostQueryEnum.mySyntax, new requisionData(new CreateHostQueryEnum())); //*
		fieldMap.put(CreateRenterQueryEnum.mySyntax, new requisionData(new CreateRenterQueryEnum())); //*
		fieldMap.put(CreateListingQueryEnum.mySyntax, new requisionData(new CreateListingQueryEnum())); //*
		fieldMap.put(CreateAvailableDateQueryEnum.mySyntax, new requisionData(new CreateAvailableDateQueryEnum())); //*
		fieldMap.put(CreateAmmmQueryEnum.mySyntax, new requisionData(new CreateAmmmQueryEnum())); //*
		fieldMap.put(BookListingQueryEnum.mySyntax, new requisionData(new BookListingQueryEnum()));
		
		fieldMap.put(DeleteUser.mySyntax, new requisionData(new DeleteUser())); //*
		fieldMap.put(DeleteListing.mySyntax, new requisionData(new DeleteListing())); //*
		fieldMap.put(DeleteIdleDate.mySyntax, new requisionData(new DeleteIdleDate())); //*
		
		fieldMap.put(UpdateIdlePrice.mySyntax, new requisionData(new UpdateIdlePrice())); //*
		fieldMap.put(CancelBookedListingQuery.mySyntax, new requisionData(new CancelBookedListingQuery())); //*
		
		fieldMap.put(ShowPotentialCommertial.mySyntax, new requisionData(new ShowPotentialCommertial())); //* 
		fieldMap.put(ShowListingNum.mySyntax, new requisionData(new ShowListingNum())); //* 
		fieldMap.put(ShowListingWithAmmm.mySyntax, new requisionData(new ShowListingWithAmmm())); //* 
		fieldMap.put(ShowHostOwning.mySyntax, new requisionData(new ShowHostOwning())); //*
		fieldMap.put(ShowBelonging.mySyntax, new requisionData(new ShowBelonging())); //*
		fieldMap.put(ShowRenters.mySyntax, new requisionData(new ShowRenters()));//*
		fieldMap.put(ShowBookAt.mySyntax, new requisionData(new ShowBookAt())); //**
		fieldMap.put(ShowTotalListing.mySyntax, new requisionData(new ShowTotalListing()));
		fieldMap.put(ShowWorstPerson.mySyntax, new requisionData(new ShowWorstPerson()));
		
		fieldMap.put(ShowRenterComment.mySyntax, new requisionData(new ShowRenterComment()));
		
		fieldMap.put(SQLSHOW.mySyntax, new requisionData(new SQLSHOW())); //*
		fieldMap.put(SQLUPDATE.mySyntax, new requisionData(new SQLUPDATE())); //*
		
	}

	public Query getQuery(String syntaxx, Scanner scanner, Instruction instruction) {
		requisionData rdata = fieldMap.get(syntaxx);
		if (rdata != null) {
			Console.logln(instruction.incompleteQuery());
			return rdata.myQueryInst.execute(Controller.fieldsFiller(rdata.getFields(), scanner));
		} else {
			return null;
		}
	}
}
