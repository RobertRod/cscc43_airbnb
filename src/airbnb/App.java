package airbnb;

import instruction.Instruction;

public class App {

	public static void main(String args[]) {
		Console console = new Console();
		Instruction instruction = Instruction.init();
		
		// Prepare the console
		try {
			console.init();
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println(instruction.databaseInitFailure());
			System.exit(-1);
		}
		
		// Start the console
		Console.log(instruction.welcome());
		Console.log(instruction.manual());
		if (console.mainLoop() == 0) {
			System.out.printf(instruction.goodbye());
		}
		else {
			System.out.printf(instruction.consoleCrash());
		}
		
		// end of the program
	}
	
	
}
