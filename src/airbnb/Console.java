package airbnb;

import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;

import demoPurpose.FakeDataGenerator;
import instruction.Instruction;
import instruction.Macros;
import query.Controller;
import query.PackedQuery;
import query.Query;

public class Console {
	private static Scanner stdin;
	private static Instruction instruction;
	private static DataBase database;
	private static Controller controller;
	
	/*
	 * Initialize all required class instance at the start of the program
	 */
	public void init() throws ClassNotFoundException, SQLException, NoSuchAlgorithmException {
		// TODO: other instances to be added
		stdin = new Scanner(System.in);
		instruction = Instruction.init();
		database = new DataBase("root", "1852856");
		controller = Controller.getInstance(stdin);
	}
	
	public void sendToDataBase(Query q) throws SQLException {
		if(q == null) {
			return;
		}
		for (PackedQuery packedQ : q.packQuery()) {
			if (packedQ.getType() == Macros.normalSetDataQuery) {
				try {
					database.updateDatabase(packedQ.getQuery());
				}
				catch (Exception e1) {
					continue;
				}
			}
			else if (packedQ.getType() == Macros.normalGetDataQuery) {
				try {
					ResultSet rs = database.executeQuery(packedQ.getQuery());
					while(rs.next()) {
						int i = 1;
						while(i >= -1) {
							try {
								Console.log(rs.getObject(i).toString() + "\t");
								++i;
							}
							catch(Exception e) {
								Console.logln("");
								break;
							}
						}
					}
				}
				catch (Exception e2) {
					continue;
				}
			}
			else if (packedQ.getType() == Macros.riskySetDataQuery) {
				database.updateDatabase(packedQ.getQuery());
			}
			else if (packedQ.getType() == Macros.riskyGetDataQuery) {
				database.executeQuery(packedQ.getQuery());
			}
		}
	}
	
	/*
	 * Program loop that keep taking user input and perform relevant operation
	 * Return 0 on regular exit and -1 otherwise.
	 */
	public int mainLoop() {
		while (true) {
			System.out.print(">> ");
			String userInp = stdin.nextLine();
			
			if (userInp.equals("exit")) {
				break;
			}
			if (userInp.equals("help")) {
				Console.log(instruction.manual());
				continue;
			}
			if (userInp.equals("1145141919810")) {
				FakeDataGenerator.JustDOIT();
				continue;
			}
			if (userInp.equals("price suggestion")) {
				Console.logln("What is the price you are willing to offer in the case that you offer no extra amenity: ");
				Float initial = stdin.nextFloat();
				Console.logln("How many amenities are you willing to offer: ");
				int count = stdin.nextInt();
				Console.logln("The price we suggest is " + initial + count*20);
			}
			// Pass the user input to the controller
			Query q = controller.matchInput(userInp);
			if (q == null) {
				System.out.printf(instruction.invalidSyntax());
			}
			else {
				// All necessary information are present at this stage
				try {
					this.sendToDataBase(q);
				}
				catch (Exception e) {
					e.printStackTrace();
//					Console.logln(e.getMessage());
					System.out.printf(instruction.conflictOperation());
					continue;
				}
			}
			System.out.printf(instruction.validStep());
		}
		return 0;
	}
	public static void logln(Object obj) {
		System.out.println(obj);
		System.out.flush();
	}
	
	public static  void log(Object obj) {
		System.out.print(obj);
		System.out.flush();
	}
	
	public static String getMD5(String intake) {
		return database.makeMD5(intake);		
	}
	public static GregorianCalendar getDate(String intake) {
		return database.makeDate(intake);		
	}
	public static ArrayList<GregorianCalendar> getDateInterval(GregorianCalendar start, GregorianCalendar end) {
		return database.makeDateInterval(start, end);	
	}
	public static ResultSet directDBAcquire(String command) throws SQLException {
		return database.executeQuery(command);
	}
	public static void directDBManipulate(String command) throws SQLException {
		database.updateDatabase(command);
	}
}
