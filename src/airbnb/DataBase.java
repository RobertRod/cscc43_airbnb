package airbnb;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import javax.xml.bind.DatatypeConverter;

/**
 * @author Qingtian Wang . Making Our Own Database Object for easier operations
 *         and instantiation
 */
public class DataBase {

	private Connection myConnection;
	private String username;
	private String password;
	private MessageDigest md;

	final String dataBaseName = "airBNBsim";
	final String driver = "com.mysql.cj.jdbc.Driver";
	final String dataBaseURL = "jdbc:mysql://localhost:3306/" + dataBaseName
			+ "?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true";

	/**
	 * @param usernameN
	 *            the username for logging in SQL server, for verification
	 * @param passwordN
	 *            the password for logging in SQL server, for verification
	 * @throws SQLException
	 *             Connection to sql database failed.
	 * @throws ClassNotFoundException
	 *             The class com.mysql.jdbc.Driver has not been imported.
	 * @throws NoSuchAlgorithmException
	 * 			   The MD5 hash algorithm cannot be found.
	 */
	public DataBase(String usernameN, String passwordN)
			throws SQLException, ClassNotFoundException, NoSuchAlgorithmException {
		username = usernameN;
		password = passwordN;
		Class.forName(driver);
		myConnection = DriverManager.getConnection(dataBaseURL, username, password);
		if (myConnection.isClosed()) {
			throw new SQLException();
		}
		Statement myStatement;
		myStatement = myConnection.createStatement();
		md = MessageDigest.getInstance("MD5");
		String[] tables = {
				"listing (MD5 CHAR(32) PRIMARY KEY," // Uses MD5 to store as key
						+ "roomAddress BLOB(8191) NOT NULL, " 
						+ "latitude FLOAT NOT NULL, " 
						+ "longitude FLOAT NOT NULL, "
						+ "house_type ENUM ('House', 'Apartment', 'Bed and breakfast', 'Bungalow', 'Guest suite', 'Guesthouse', 'Loft', 'Townhouse', 'Villa', 'Room') )", // change enum to list and change elements inside
				
				"amenities (MD5 CHAR(32), " 
						+ "amen ENUM ('Kitchen','Shampoo','Heating','Air conditioning','Washer','Dryer','Wifi','Breakfast','Indoor fireplace','Hangers','Iron','Hair dryer','Laptop-friendly workspace','TV','Crib','High chair','Self check-in','Smoke detector','Carbon monoxide detector','Private bathroom'), " 
						+ "CONSTRAINT onListingAM FOREIGN KEY (MD5) REFERENCES listing(MD5) ON DELETE CASCADE,"
						+ "INDEX(MD5))",
						
				"user (sin INT UNSIGNED PRIMARY KEY, " 
						+ "birthday VARCHAR(255), " 
						+ "address BLOB(8191)," 
						+ "name VARCHAR(255) NOT NULL,"
						+ "job VARCHAR(255), " 
						+ "cardNum BIGINT UNSIGNED, "
						+ "uType ENUM ('renter', 'host') NOT NULL, "
						+ "INDEX(sin))",	
						
				"own (sin_host INT UNSIGNED NOT NULL, "
						+ "MD5 CHAR(32) NOT NULL, "
						+ "CONSTRAINT theOwner FOREIGN KEY (sin_host) REFERENCES user(sin) ON DELETE CASCADE, "
						+ "CONSTRAINT ownedListing FOREIGN KEY (MD5) REFERENCES listing(MD5) ON DELETE CASCADE, "
						+ "INDEX(sin_host))", 

				"availability_Calendar (MD5 CHAR(32) NOT NULL, "
						+ "dateAvailable DATE NOT NULL, "
						+ "price FLOAT, "
						+ "CONSTRAINT onListingA FOREIGN KEY (MD5) REFERENCES listing(MD5) ON DELETE CASCADE, "
						+ "INDEX(dateAvailable))", 
				
				"booking_Calendar (MD5 CHAR(32) NOT NULL, "
						+ "booked_date DATE NOT NULL, "
						+ "booked_price FLOAT, "
						+ "booker_sin INT UNSIGNED NOT NULL, "
						+ "INDEX (booker_sin), "
						+ "CONSTRAINT onListingB FOREIGN KEY (MD5) REFERENCES listing(MD5) ON DELETE CASCADE)",
						
				"book_cancel_history (MD5 CHAR(32) NOT NULL, "
						+ "booked_date DATE NOT NULL, "
						+ "booked_price FLOAT, "
						+ "booker_sin INT UNSIGNED NOT NULL, "
						+ "performed_by ENUM ('Host', 'Renter') NOT NULL)",
						
				"comment ("
						+ "commenterSIN INT UNSIGNED NOT NULL, "
						+ "commenteeSIN INT UNSIGNED NOT NULL, "
						+ "rating ENUM ('0', '1', '2', '3', '4', '5') NOT NULL, "
						+ "comment BLOB(8191), "
						+ "CONSTRAINT whoGotComment FOREIGN KEY (commenteeSIN) REFERENCES user(sin), "
						+ "CONSTRAINT whoSendComment FOREIGN KEY (commenterSIN) REFERENCES user(sin) ON DELETE CASCADE)", 
						
				"general_location ("
						+ "MD5 CHAR(32) NOT NULL, "
						+ "city ENUM ('Toronto', 'Vancouver', 'Ottawa', 'Scarborough', 'North York', 'Shimo-kitazawa') NOT NULL, "
						+ "country ENUM ('Canada', 'Japan') NOT NULL, "
						+ "INDEX (city), "
						+ "CONSTRAINT listingCity FOREIGN KEY (MD5) REFERENCES listing(MD5) ON DELETE CASCADE)",
						
//				"paymentInfo ("
//						+ "cardNumber BIGINT UNSIGNED PRIMARY KEY, "
//						+ "renterSin INT UNSIGNED NOT NULL, "
//						+ "billAddress BLOB(8191) NOT NULL, "
//						+ "cvcMD5 CHAR(128) NOT NULL, " // Encrypted to ensure no one can steal the cvc number >:D (not really useful tho)
//						+ "dateOfExpire DATE, "
//						+ "CONSTRAINT whosCard FOREIGN KEY (renterSin) REFERENCES user(sin) ON DELETE CASCADE)"
					
				// "renter (sin CHAR(9) PRIMARY KEY"
				// + ")",
				// "hoster (sin CHAR(9) PRIMARY KEY"
				// + ")",
//						"rent (renterSin INT UNSIGNED NOT NULL, "
//						+ "listingMD5 CHAR(128) NOT NULL, "
//						+ "rentStartDate DATE NOT NULL, "
//						+ "rentEndDate DATE, "
//						+ "CONSTRAINT renter FOREIGN KEY (renterSin) REFERENCES user(sin), "
//						+ "CONSTRAINT targetList FOREIGN KEY (listingMD5) REFERENCES listing(roomAddressMD5))",
				"commentOnListing (commentLID INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,"
						+ "renterSin INT UNSIGNED NOT NULL, "
						+ "listingMD5 CHAR(128) NOT NULL, "
						+ "rating ENUM ('0', '1', '2', '3', '4', '5') NOT NULL, "
						+ "comment BLOB, "
						+ "CONSTRAINT commentedListing FOREIGN KEY (listingMD5) REFERENCES listing(MD5) ON DELETE CASCADE, "
						+ "CONSTRAINT byThatRenter FOREIGN KEY (renterSin) REFERENCES user(sin))", 
		};
		for (String table : tables) {
//			System.out.println(table);
			myStatement.executeUpdate("CREATE TABLE IF NOT EXISTS " + table + " ENGINE=InnoDB DEFAULT CHARSET=utf8;");
//			System.out.println("");
		}
//		myStatement.executeUpdate("ALTER TABLE user ADD CONSTRAINT defaultlyUsedBankCard FOREIGN KEY (defaultBankCard) REFERENCES paymentInfo(cardNumber);"); 
		// Upper line may have errors, please pay attention. 
		myStatement.close();
		myConnection.close();
	}

	/**
	 * @param query
	 *            Wrapped query data type that represent a syntatically correct SQL
	 *            command that want to be executed
	 * @return the query result in ResultSet
	 * @throws SQLException
	 *             Connection to sql database failed.
	 */
	public ResultSet executeQuery(String query) throws SQLException {
		myConnection = DriverManager.getConnection(dataBaseURL, username, password);
		if (myConnection.isClosed()) {
			throw new SQLException();
		}
		Statement myStatement;
		myStatement = myConnection.createStatement();
//		Console.logln(query + "  JUSTTT BEFORE");
		ResultSet resultt = myStatement.executeQuery(query);
//		myConnection.close();
		return resultt;
	}

	/**
	 * @param command
	 *            SQL command that want to be executed
	 * @throws SQLException
	 *             Connection to sql database failed.
	 */
	public void updateDatabase(String command) throws SQLException {
		myConnection = DriverManager.getConnection(dataBaseURL, username, password);
		if (myConnection.isClosed()) {
			throw new SQLException();
		}
		Statement myStatement;
		myStatement = myConnection.createStatement();
//		Console.logln(command + "  JUST BEFORE");
		myStatement.executeUpdate(command);
		myStatement.close();
		myConnection.close();
	}

	public String makeMD5(String targetString) {
		md.update(targetString.getBytes());
		byte[] digest = md.digest();
		String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
		return myHash;
	}
	
	public GregorianCalendar makeDate(String raw) {
		raw = raw.replaceAll("-", "");
		int year = Integer.parseInt(raw.substring(0, 4));
		int month = Integer.parseInt(raw.substring(4, 6)) - 1;
		int day = Integer.parseInt(raw.substring(6, 8));
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(year, month, day);
		return cal;
	}
	
	public ArrayList<GregorianCalendar> makeDateInterval(GregorianCalendar start, GregorianCalendar end) {
		end.add(end.DATE, 1);
		if(start.after(end)) {
			return makeDateInterval(end, start);
		}
		else {
			ArrayList<GregorianCalendar> dates = new ArrayList<GregorianCalendar>();
			for(int i = 0; start.before(end); ++i) {
				GregorianCalendar anotherDate = (GregorianCalendar) start.clone();
				anotherDate.add(anotherDate.DATE, i);
				dates.add(anotherDate);
				end.add(end.DATE, -1);
			}
			return dates;
		}
	}
}
