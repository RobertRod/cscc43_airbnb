package airbnb;

public enum AcquisionFields {
	SIN, BIRTHDAY, ADDRESS(true), NAME, JOB, CARDNUM, UTYPE, 
	MD5, ROOMADDRESS(true), LATITUDE, LONGITUDE, HOUSE_TYPE, 
	AMEN,
	SIN_HOST, 
	DATEAVAILABLE, PRICE,
	BOOKED_DATE, BOOKED_PRICE, BOOKER_SIN,  
	PERFORMED_BY,
	COMMENTERSIN, COMMENTEESIN, RATING, COMMENT(true),
	CITY, COUNTRY,	
	COMMENTLID, RENTERSIN, LISTINGMD5;
	public boolean isBlob;
	AcquisionFields(boolean isit) {
		this.isBlob=isit;
		}
	AcquisionFields() {
		this.isBlob=false;
		}
}
