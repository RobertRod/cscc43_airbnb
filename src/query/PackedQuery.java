package query;

public class PackedQuery {
	String query;
	int type; // 0 for reterive data query, 1 for set data query, 2 for set data query that require exit on failure
	
	public PackedQuery(String q, int t) {
		this.query = q;
		this.type = t;
	}
	
	public String getQuery() {
		return this.query;
	}
	
	public int getType() {
		return this.type;
	}
}
