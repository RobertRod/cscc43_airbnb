package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

public class DeleteListing extends QueryEnum {
	public final static String mySyntax = "delete listing";
	
	public enum myFields{
		SIN, MD5
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		String inpSIN = hashMap.get(neededFields()[0]);
		String inpMD5Packed = textPackager(hashMap.get(neededFields()[1]));
		boolean verified = verf(inpSIN, inpMD5Packed);
		if(verified) {
			AcquisionFields[] aFields = { AcquisionFields.BOOKED_PRICE, AcquisionFields.MD5, AcquisionFields.BOOKER_SIN, AcquisionFields.BOOKED_DATE };

			ArrayList<HashMap> aqq = Acquisition.acquire(aFields, "booking_Calendar", inpMD5Packed + " = md5 AND TO_DAYS(booked_date) - TO_DAYS(now()) > 0");
			
			for(HashMap<AcquisionFields, Object> hm : aqq) {
				CancellationRecorder.cancellationRecord(
						inpMD5Packed,
						textPackager(hm.get(AcquisionFields.BOOKED_DATE).toString()), 
						hm.get(AcquisionFields.BOOKED_PRICE).toString(), 
						hm.get(AcquisionFields.BOOKER_SIN).toString(), 
						CancellationRecorder.whoDid.host);
			}
			
			
			PackedQuery myPackedQuery = new PackedQuery("DELETE FROM listing WHERE MD5 = " + inpMD5Packed + ";"
					, 1);
			justPQ.add(myPackedQuery);
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
					
			return jqp;
		}
		else {
			Console.logln("Either the listing or the host does not exist, deletion denied. ");
			return null;
		}
	}

	
	private boolean verf(String sin, String md5) {

		AcquisionFields[] aFields1 = {AcquisionFields.MD5};
		return (Acquisition.acquire(aFields1, "own", "(sin_host = " + sin + ") && " + "MD5 = " + md5).size() == 1);
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = {
				myFields.SIN.toString().toLowerCase(),
				myFields.MD5.toString().toLowerCase(),
		};
		return stuffs;
	}

}
