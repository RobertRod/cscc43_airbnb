/**
 * 
 */
package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

/**
 * @author wang_w571
 *
 */
public class CreateCommentToRenter extends QueryEnum {
	public final static String mySyntax = "new comment by host";
	
	public enum myFields{
		Your_SIN, Renter_SIN, RATING, COMMENT
	}
	
	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		String[] stuffs = {
				hashMap.get(neededFields()[0]), 
				hashMap.get(neededFields()[1]), 
				textPackager(hashMap.get(neededFields()[2])), 
				textPackager(hashMap.get(neededFields()[3])), 
		};
		boolean verified = verf(hashMap.get(neededFields()[0]), hashMap.get(neededFields()[1]));
		if(verified) {
			Console.logln("here");
			PackedQuery myPackedQuery = new PackedQuery("INSERT INTO comment (commenterSIN, commenteeSIN, rating, comment)" + 
					" VALUES " + bracketCommaMaker(stuffs)
					, 1);
			justPQ.add(myPackedQuery);
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
					
			return jqp;
		}
		else {
			return null;
		}
	}
	
	boolean verf(String host, String renter){
		AcquisionFields[] aFields = {AcquisionFields.BOOKER_SIN, AcquisionFields.SIN_HOST};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "booking_Calendar b LEFT JOIN own o ON o.MD5 = b.MD5", host+" = sin_host AND " + renter +" = booker_sin");
		return rslt.size() != 0;
	}
	
	@Override
	public String[] neededFields() {
		String[] stuffs = {
				myFields.Your_SIN.toString().toLowerCase(),
				myFields.Renter_SIN.toString().toLowerCase(),
				myFields.RATING.toString().toLowerCase(),
				myFields.COMMENT.toString().toLowerCase()
		};
		return stuffs;
	}

}
