package query;

import java.util.ArrayList;
import java.util.HashMap;

/*
 * The super class for all queries supported by airbnb console.
 * Every Query object is required to have a syntactically correct string representation
 * with respect to the SQL query grammar.
 */
public class Query {
	
	// By agreement, all query should have several fields needs to be filled in on initialization
	protected HashMap<String, String> infos = new HashMap<>();
	
	public Query() {}
	
	/*
	 * Get the sql query that perform the desired operation
	 */
	public ArrayList<PackedQuery> packQuery() {
		return null;
	}

	
	/*
	 * Get the command that users need to enter to create this query
	 */
	public static String getSyntax() {
		return null;
	}
	
	
	/*
	 * Return the value according to the provided field name
	 */
	protected String getField(String field) {
		return this.infos.get(field);
	}
	

	/*
	 * Check if this query is ready to be passed to a sql database.
	 * Return the field names that are not completed
	 */
	public boolean isComplete(ArrayList<String> exclude) {
		for (String key : this.infos.keySet()) {
			if (!exclude.contains(key) && this.infos.get(key) == null) {
				return false;
			}
		}
		return true;
	}
	
	
}
