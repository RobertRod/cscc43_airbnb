package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

public class ShowHostOwning extends QueryEnum {

	public final static String mySyntax = "show owning";

	public enum myFields {
		Host_SIN
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {		
		String hostSIN = textPackager(hashMap.get(neededFields()[0]));
		if(verified(hostSIN)) {
			Console.logln("Listings that belongs to " + hostSIN + "are:");
			
			ArrayList<HashMap> rslt = everyStuff(hostSIN);
			
			// For logging
			for(HashMap<AcquisionFields, Object> hm : rslt) {
				Console.log(hm.get(AcquisionFields.MD5) + "\t\t");
				Console.logln(hm.get(AcquisionFields.ROOMADDRESS));
			}
			
			// To confuse the console
			ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
			return jqp;
		}
		else {
			Console.logln("The host with given sin cannot be found.");
		}
		return null;
	}

	public ArrayList<HashMap> everyStuff(String hostSIN) {
		// For acquiring data
		AcquisionFields[] aFields = {AcquisionFields.SIN_HOST, AcquisionFields.MD5, AcquisionFields.ROOMADDRESS};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields," sin_host, o.MD5, roomaddress ",  "own o JOIN listing l ON o.MD5 = l.MD5", "sin_host = " + hostSIN);
		return rslt;
	}

	private boolean verified(String sin) {
		AcquisionFields[] aFields = {AcquisionFields.SIN};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "user", sin +" = sin " + " AND (uType = 'host')");
		return rslt.size() == 1;
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = { 
				myFields.Host_SIN.toString()
				};
		return stuffs;
	}

}
