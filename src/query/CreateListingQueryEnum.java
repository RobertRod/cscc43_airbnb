package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

public class CreateListingQueryEnum extends QueryEnum {
	public final static String mySyntax = "new listing";
	
	public enum myFields{
		roomAddress, latitude, longitude, house_type, sin_host
	}
//	public static requisionData queryData = new requisionData(CreateCommentToRenter.class, fields.values());
	/**
	 * 利用enum减少String的使用量
	 */
	
	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		String md5 = Console.getMD5(hashMap.get(neededFields()[0]));
		
		String inpAddress = hashMap.get(neededFields()[0]);
		String inpLatitude = hashMap.get(neededFields()[1]);
		String inpLongitude = hashMap.get(neededFields()[2]);
		String inpType = hashMap.get(neededFields()[3]);
		String inpSIN_host = hashMap.get(neededFields()[4]);
				
		boolean verified = verf(inpSIN_host);
		if(verified) {
			String[] houseInfo = {
					textPackager(md5),
					textPackager(inpAddress), 
					textPackager(inpLatitude), 
					textPackager(inpLongitude), 
					textPackager(inpType), 
			};
			String[] hostInfo = {
					textPackager(inpSIN_host),
					textPackager(md5),
			};
			PackedQuery myPackedQuery1 = new PackedQuery("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type)" + 
					" VALUES " + bracketCommaMaker(houseInfo)
					, 1);
			PackedQuery myPackedQuery2 = new PackedQuery("INSERT INTO own (sin_host, MD5)" +
					" VALUES " + bracketCommaMaker(hostInfo)
					, 1);
//			Console.logln(myPackedQuery1.query);
//			Console.logln(myPackedQuery2.query);
			justPQ.add(myPackedQuery1);
			justPQ.add(myPackedQuery2);
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
					
			return jqp;
		}
		else {
			Console.logln("Either: This listing address already exist, the user does not exist, or the user is not a host. "
					+ "\nCreation denied. ");
			return null;
		}
	}
	
	private boolean verf(String sin) {
		AcquisionFields[] aFields1 = {AcquisionFields.SIN};
		return (Acquisition.acquire(aFields1, "user", "(sin = " + sin + ")" + " AND (uType = 'host')").size() == 1); //&&
				//(Acquisition.acquire(aFields2, "listing", "MD5 = " + "'" + md5 + "'").size() >= 1);
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = {
				myFields.roomAddress.toString(),
				myFields.latitude.toString(),
				myFields.longitude.toString(),
				myFields.house_type.toString(),
				myFields.sin_host.toString()
		};
		return stuffs;
	}
}
