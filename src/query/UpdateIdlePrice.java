package query;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;

import airbnb.Console;

public class UpdateIdlePrice extends QueryEnum {

	public final static String mySyntax = "update idle";

	public enum myFields {
		MD5, fromYYYYMMDD, toYYYYMMDD, price
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		
		GregorianCalendar startDate = Console.getDate(hashMap.get(neededFields()[1]));
		GregorianCalendar endDate = Console.getDate(hashMap.get(neededFields()[2]));
		
		String roomMD5 = textPackager(hashMap.get(neededFields()[0]));
		String price = hashMap.get(neededFields()[3]);
		
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();		
		// Do updating
		for (GregorianCalendar oneDate : Console.getDateInterval(startDate, endDate)) {
			PackedQuery myPackedQuery = new PackedQuery(
					"UPDATE availability_Calendar SET price = "+ price + " WHERE dateAvailable = "+ sdf.format(oneDate.getTime()).toString() + 
					" AND MD5 = " + roomMD5 + ";",
					1);
			
			justPQ.add(myPackedQuery);
		}
		// Pack up and ready to execute
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = { 
				myFields.MD5.toString(), 
				myFields.fromYYYYMMDD.toString(), 
				myFields.toYYYYMMDD.toString(),
				myFields.price.toString() };
		return stuffs;
	}

}
