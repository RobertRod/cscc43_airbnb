package query;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;



//import com.mysql.cj.jdbc.Blob;

import airbnb.AcquisionFields;
import airbnb.Console;

public class ShowBookAt extends QueryEnum {
	
	public final static String mySyntax = "show books";
	
	public final static String thisManual = "Use [Graphical location(longitude and latitude)] or [Graphical location of a listing]"
			+ " or [a city] to give a location to find listing beside. Also, if time is offered, result will also be filtered with time."
			+ " Note that if multiples location filters were given,"
			+ " the first non-empty attribute will be used and behind attributes will be ignored. ";

	public enum myFields {
		LONGITUDE, LATITUDE, Address_MD5, RANGE_OF_TUDES, CITY, BOOK_DATE_FROM, BOOK_DATE_TO
	}
	private float allowedDifference = 0.5f;

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		Console.logln("With filters you provided that are not empty, we found results below: ");

		// For acquiring data
		allowedDifference = hashMap.get(neededFields()[3]).trim().equals("")?0.5f:Float.parseFloat(hashMap.get(neededFields()[3]).trim());
		ArrayList<HashMap> rslt = everyStuff(hashMap);

		// For logging
		for (HashMap<AcquisionFields, Object> hm : rslt) {
				Console.log(hm.get(AcquisionFields.BOOKED_DATE));
				Console.log("\t");
				Console.log(hm.get(AcquisionFields.ROOMADDRESS));			
				Console.log("\t");
				Console.log("\t");
				Console.log("\t");
				Console.logln(hm.get(AcquisionFields.MD5));
			
		}

		// To confuse the console
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;
	}
	
	public ArrayList<HashMap> everyStuff(HashMap<String, String> hashMap) {
		AcquisionFields[] aFields = { AcquisionFields.BOOKED_DATE, AcquisionFields.MD5, AcquisionFields.ROOMADDRESS};
		String latitude = hashMap.get(neededFields()[0]).toString();
		String longitude = hashMap.get(neededFields()[1]).toString();
		String roomMD5 = hashMap.get(neededFields()[2]).toString();
		String city = hashMap.get(neededFields()[4]).toString().replaceAll("\\s+$", "");
		
		String dateF = hashMap.get(neededFields()[5]).trim().equals("")?"19000101":hashMap.get(neededFields()[5]).trim();
		String dateT = hashMap.get(neededFields()[6]).trim().equals("")?"29991231":hashMap.get(neededFields()[6]).trim();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		
		GregorianCalendar startDate = Console.getDate(dateF);
		GregorianCalendar endDate = Console.getDate(dateT);
		
		
		if(!longitude.trim().equals("") && !latitude.trim().equals("")) {
			Float lof = Float.parseFloat(longitude);
			Float laf = Float.parseFloat(latitude);
			// L&L
			return Acquisition.acquire(aFields,
					"BOOKED_DATE, g.MD5, roomAddress ", 
					"booking_Calendar b JOIN listing g ON b.MD5 = g.MD5",
					"(longitude BETWEEN " + String.valueOf(lof-allowedDifference) + " AND " + String.valueOf(lof+allowedDifference) + ") AND "
							+ "(latitude BETWEEN "+ String.valueOf(laf-allowedDifference) +" AND " + String.valueOf(laf+allowedDifference) + ")"
									+ " AND (TO_DAYS(BOOKED_DATE) BETWEEN TO_DAYS("+sdf.format(startDate.getTime())+") AND TO_DAYS(" +sdf.format(endDate.getTime())+"))");
		}
		else if(!roomMD5.trim().equals("")) {
			// LOC
			AcquisionFields[] afaf = { AcquisionFields.LONGITUDE, AcquisionFields.LATITUDE };
			ArrayList<HashMap> acquirererere = Acquisition.acquire(afaf, "listing", "MD5 = " + textPackager(roomMD5));
			if(acquirererere.size() == 0) {
				return null;
			}
			HashMap<AcquisionFields, Object> hm = acquirererere.get(0);
			Float lof = Float.parseFloat(hm.get(AcquisionFields.LONGITUDE).toString());
			Float laf = Float.parseFloat(hm.get(AcquisionFields.LATITUDE).toString());
			return Acquisition.acquire(aFields,
					"BOOKED_DATE , g.MD5, roomAddress ",
					"booking_Calendar b JOIN listing g ON b.MD5 = g.MD5 ",
					"(longitude BETWEEN " + String.valueOf(lof-allowedDifference) + " AND " + String.valueOf(lof+allowedDifference) + ") AND "
							+ "(latitude BETWEEN "+ String.valueOf(laf-allowedDifference) +" AND " + String.valueOf(laf+allowedDifference) + ")"
									+ " AND (TO_DAYS(BOOKED_DATE) BETWEEN TO_DAYS("+sdf.format(startDate.getTime())+") AND TO_DAYS(" +sdf.format(endDate.getTime())+"))" );
		}
		else if(!city.equals("")) {
			return Acquisition.acquire(aFields,
					"BOOKED_DATE , g.MD5, roomAddress ",
					"(booking_Calendar b JOIN general_location g ON b.MD5 = g.MD5 "
					+ "JOIN listing l ON l.MD5 = g.MD5)", 
					"g.city = " + textPackager(city) + " AND (TO_DAYS(BOOKED_DATE) BETWEEN TO_DAYS("+sdf.format(startDate.getTime())+") AND TO_DAYS(" +sdf.format(endDate.getTime())+"))");
			
		}
		else {
			return Acquisition.acquire(aFields,
					"BOOKED_DATE , g.MD5, roomAddress ",
					"(booking_Calendar b JOIN general_location g ON b.MD5 = g.MD5 "
					+ "JOIN listing l ON l.MD5 = g.MD5)", 
					"(TO_DAYS(BOOKED_DATE) BETWEEN TO_DAYS("+sdf.format(startDate.getTime())+") AND TO_DAYS(" +sdf.format(endDate.getTime())+"))");
		}		
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = { myFields.LONGITUDE.toString(), myFields.LATITUDE.toString(),
				myFields.Address_MD5.toString(), myFields.RANGE_OF_TUDES.toString(), myFields.CITY.toString(), myFields.BOOK_DATE_FROM.toString(), myFields.BOOK_DATE_TO.toString()  };
		return stuffs;
	}

}
