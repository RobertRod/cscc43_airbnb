package query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import airbnb.AcquisionFields;
import airbnb.Console;
import query.ShowHostOwning.myFields;

public class ShowWorstPerson extends QueryEnum {

	public final static String mySyntax = "show user with most cancellation";

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		// String amenityPacked = textPackager(hashMap.get(neededFields()[0]));

		Console.logln("Please ignore the line above :D \r\n \n");

		ArrayList<HashMap> rslt = everyStuff();

		TreeMap<String, Integer> countR = new TreeMap<String, Integer>();
		TreeMap<String, Integer> countH = new TreeMap<String, Integer>();
		int fullcount = 0;
		for (HashMap<AcquisionFields, Object> hm : rslt) {
			String thisSin;
			if (hm.get(AcquisionFields.PERFORMED_BY).toString().toLowerCase().trim().equals("host")) {
				thisSin = hm.get(AcquisionFields.SIN_HOST).toString();
				if (countH.containsKey(thisSin)) {
					countH.replace(thisSin, countH.get(thisSin) + 1);
				} else {
					countH.put(thisSin, 1);
				}
			} else {
				thisSin = hm.get(AcquisionFields.BOOKER_SIN).toString();
				if (countR.containsKey(thisSin)) {
					countR.replace(thisSin, countR.get(thisSin) + 1);
				} else {
					countR.put(thisSin, 1);
				}
			}
			++fullcount;
		}
		if (countR.size() != 0) {
			for (String thatRenter : countR.keySet()) {
				if (countR.get(thatRenter) < (int) countR.values().toArray()[countR.values().size() - 1]) {
					countR.remove(thatRenter);
				}
			}
			String thatRenter = countR.firstKey();
			Console.logln(thatRenter + " cancelled " + countR.get(thatRenter).toString()
					+ " bookings, having the highest cancellation rate among all renters.");

		}
		if (countH.size() != 0) {
			for (String thatHost : countH.keySet()) {
				int thatHostCancelled = countH.get(thatHost);
				int most = (int) countH.values().toArray()[countH.values().size() - 1];
				if (thatHostCancelled < most) {
					countH.remove(thatHost);
				}
			}
			String thatHost = countH.firstKey();
			Console.logln(thatHost + " cancelled " + countH.get(thatHost).toString()
					+ " booking, having the highest cancellation rate among all hosts.");
		}
		// To confuse the console
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;
	}

	public ArrayList<HashMap> everyStuff() {
		// For acquiring data
		AcquisionFields[] aFields = { AcquisionFields.BOOKER_SIN, AcquisionFields.SIN_HOST,
				AcquisionFields.PERFORMED_BY };
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "book_cancel_history b JOIN own o ON o.md5 = b.md5",
				"TO_DAYS(now()) - TO_DAYS(booked_date) < 365");
		return rslt;
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = {};
		return stuffs;
	}

}
