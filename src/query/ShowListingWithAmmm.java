package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;
import query.ShowHostOwning.myFields;

public class ShowListingWithAmmm extends QueryEnum {

	public final static String mySyntax = "show listing with amenity";

	public enum myFields {
		AMENITY
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {		
		String amenityPacked = textPackager(hashMap.get(neededFields()[0]));
		
		Console.logln("Listings that have " + amenityPacked + " are:");
		
		ArrayList<HashMap> rslt = everyStuff(amenityPacked);
		
		// For logging
		for(HashMap<AcquisionFields, Object> hm : rslt) {
			Console.log(hm.get(AcquisionFields.MD5) + "\t\t");
			Console.logln(hm.get(AcquisionFields.ROOMADDRESS));
		}
		
		// To confuse the console
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;
	}

	public ArrayList<HashMap> everyStuff(String amenityPacked) {
		// For acquiring data
		AcquisionFields[] aFields = {AcquisionFields.MD5, AcquisionFields.ROOMADDRESS};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields," a.MD5, roomaddress ",  "amenities a JOIN listing l ON a.MD5 = l.MD5", "amen = " + amenityPacked);
		return rslt;
	}

	private boolean verified(String sin) {
		AcquisionFields[] aFields = {AcquisionFields.SIN};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "user", sin +" = sin " + " AND (uType = 'host')");
		return rslt.size() == 1;
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = { 
				myFields.AMENITY.toString()
				};
		return stuffs;
	}

}
