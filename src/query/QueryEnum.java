package query;

import java.util.HashMap;

public abstract class QueryEnum {
	/**
	 * This function will be called to get a query for database to update/acquire
	 * @param hashMap the keys are attributes required by the query function, and the values are corresponding values. 
	 * @return A JustQueryPackger object that has queries to be processed
	 */
	public abstract Query execute(HashMap<String, String> hashMap);
	
	/**
	 * @param textt the text that wants to be packed
	 * @return 'targettext'
	 */
	public static String textPackager(String textt) {
		return "'" + textt + "' ";
	}
	/**
	 * @param texts the texts that were to be put in
	 * @return (text1, text2, text3) or such things
	 */
	public static String bracketCommaMaker(String[] texts) {
		String result = "(";
		int loopcount = 0;
		for (String text : texts) {
			result += text;
			if(loopcount < texts.length - 1) {
				result += ",";
			}
			++loopcount;
		}
		return result + ");";
	}
	/**
	 * @return attributes that will be used for asking
	 */
	public abstract String[] neededFields();
}
