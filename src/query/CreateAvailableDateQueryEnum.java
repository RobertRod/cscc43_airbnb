package query;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.text.SimpleDateFormat;

import airbnb.AcquisionFields;
import airbnb.Console;

public class CreateAvailableDateQueryEnum extends QueryEnum {

	public final static String mySyntax = "new idle";

	public enum myFields {
		MD5, fromYYYYMMDD, toYYYYMMDD, price, SIN
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		
		GregorianCalendar startDate = Console.getDate(hashMap.get(neededFields()[1]));
		GregorianCalendar endDate = Console.getDate(hashMap.get(neededFields()[2]));
		
		String roomMD5 = textPackager(hashMap.get(neededFields()[0]));
		String price = hashMap.get(neededFields()[3]);
		
		boolean verified = verf(hashMap.get(neededFields()[4]));
		if(verified) {
			ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();		
			
			for (GregorianCalendar oneDate : Console.getDateInterval(startDate, endDate)) {
				String[] stuffs = { 
						roomMD5, 
						sdf.format(oneDate.getTime()).toString(),
						price, 
						};
				// TODO: ��֤
				PackedQuery myPackedQuery = new PackedQuery(
						"INSERT INTO availability_calendar (MD5, dateAvailable, price)" + " VALUES " + bracketCommaMaker(stuffs), 1);
				justPQ.add(myPackedQuery);
			}
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
			return jqp;
		}
		else {
			Console.logln("The host is not recognized, creation denied. ");
			return null;
		}
	}
	
	private boolean verf(String sin) {
		//AcquisionFields[] aFields1 = {AcquisionFields.MD5};
		AcquisionFields[] aFields2 = {AcquisionFields.UTYPE};
		return (Acquisition.acquire(aFields2, "user", "sin = " + sin + " AND uType = 'host'").size() == 1);
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = { 
				myFields.MD5.toString(), 
				myFields.fromYYYYMMDD.toString(), 
				myFields.toYYYYMMDD.toString(),
				myFields.price.toString(),
				myFields.SIN.toString()};
		return stuffs;
	}
}
