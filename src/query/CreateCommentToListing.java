/**
 * 
 */
package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

/**
 * @author wang_w571
 *
 */
public class CreateCommentToListing extends QueryEnum {
	public final static String mySyntax = "new comment by renter";
	
	public enum myFields{
		Your_SIN, MD5, RATING, COMMENT
	}
	
	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		String[] stuffs = {
				hashMap.get(neededFields()[0]), 
				textPackager(hashMap.get(neededFields()[1])), 
				textPackager(hashMap.get(neededFields()[2])), 
				textPackager(hashMap.get(neededFields()[3])), 
		};
		boolean verified = verf(hashMap.get(neededFields()[0]), hashMap.get(neededFields()[1]));
		if(verified) {
			Console.logln("here");
		PackedQuery myPackedQuery = new PackedQuery("INSERT INTO commentOnListing (renterSin, listingMD5, rating, comment)" + 
				" VALUES " + bracketCommaMaker(stuffs)
				, 1);
		justPQ.add(myPackedQuery);
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
				
		return jqp;
		}
		else {
			return null;
		}
	}
	
	boolean verf(String renter, String md5){
		AcquisionFields[] aFields = {AcquisionFields.BOOKER_SIN};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "booking_Calendar", renter+" = booker_sin AND '" + md5 +"' = MD5");
		return rslt.size() != 0;
	}
	
	@Override
	public String[] neededFields() {
		String[] stuffs = {
				myFields.Your_SIN.toString().toLowerCase(),
				myFields.MD5.toString().toLowerCase(),
				myFields.RATING.toString().toLowerCase(),
				myFields.COMMENT.toString().toLowerCase()
		};
		return stuffs;
	}
	
	

}
