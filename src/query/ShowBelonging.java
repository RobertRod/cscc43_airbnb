package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

public class ShowBelonging extends QueryEnum {

	public final static String mySyntax = "show belonging";

	public enum myFields {
		MD5
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {		
		String MD5Packed = textPackager(hashMap.get(neededFields()[0]));
		if(verified(MD5Packed)) {
			Console.log("Listings " + MD5Packed + " belongs to: ");
			
			ArrayList<HashMap> rslt = everyStuff(MD5Packed);
			
			// For logging
			for(HashMap<AcquisionFields, Object> hm : rslt) {
				Console.logln(hm.get(AcquisionFields.SIN_HOST));
			}
			
			// To confuse the console
			ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
			return jqp;
		}
		else {
			Console.logln("The host with given sin cannot be found.");
		}
		return null;
	}

	public ArrayList<HashMap> everyStuff(String MD5Packed) {
		// For acquiring data
		AcquisionFields[] aFields = {AcquisionFields.SIN_HOST, AcquisionFields.MD5};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "own", "MD5 = " + MD5Packed);
		return rslt;
	}

	private boolean verified(String md5Packed) {
		AcquisionFields[] aFields = {AcquisionFields.MD5};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "listing", md5Packed +" = MD5 ");
		return rslt.size() == 1;
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = { 
				myFields.MD5.toString()
				};
		return stuffs;
	}

}
