package query;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TreeMap;
import airbnb.AcquisionFields;
import airbnb.Console;

public class ShowTotalListing extends QueryEnum {

	public final static String mySyntax = "show total listing";

	public enum myFields {
		DATE_FROM_YYYYMMDD, DATE_TO_YYYYMMDD, CITY
	}
	
	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<HashMap> rslt = everyStuff(hashMap);
		int fullcount = 0;
		for (HashMap<AcquisionFields, Object> hm : rslt) {
			++fullcount;
		}
		Console.logln("The total number of booking is: " + fullcount);
		// To confuse the console
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;
	}
	
	public ArrayList<HashMap> everyStuff(HashMap<String, String> hashMap) {
		AcquisionFields[] aFields = { AcquisionFields.BOOKER_SIN};
		
		String cityName = hashMap.get(neededFields()[2]).toString().toLowerCase().replaceAll("\\s+$", "");
		String inpStartDate = hashMap.get(neededFields()[0]).toString().toLowerCase().replaceAll("\\s+$", "");
		String inpEndDate = hashMap.get(neededFields()[1]).toString().toLowerCase().replaceAll("\\s+$", "");

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar gStartDate = Console.getDate(inpStartDate);
		GregorianCalendar gEndDate = Console.getDate(inpEndDate);
		String startDate = sdf.format(gStartDate.getTime()).toString();
		String endDate = sdf.format(gEndDate.getTime()).toString();
		
		if(!cityName.trim().equals("")) {
			return Acquisition.acquire(aFields,
					"general_location g JOIN booking_Calendar c ON g.MD5 = c.MD5 ", 
					"g.city = " + textPackager(cityName) + "AND " +
					"c.booked_date BETWEEN " + textPackager(startDate) + "AND " + textPackager(endDate));
		}
		else {
			return Acquisition.acquire(aFields,
					"booker_sin ",
					"booking_Calendar",
					"booked_date BETWEEN " + textPackager(startDate) + "AND " + textPackager(endDate));
		}		
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = { 
				myFields.DATE_FROM_YYYYMMDD.toString(),
				myFields.DATE_TO_YYYYMMDD.toString(),
				myFields.CITY.toString(), 
				};
		return stuffs;
	}

}
