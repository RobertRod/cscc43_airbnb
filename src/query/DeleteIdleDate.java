package query;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;
import query.CancellationRecorder.whoDid;

public class DeleteIdleDate extends QueryEnum {
	public final static String mySyntax = "delete idle";
	
	public enum myFields{
		MD5, DATEFROM, DATETO
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		String md5Packed = textPackager(hashMap.get(neededFields()[0]));
		String dateF = hashMap.get(neededFields()[1]);
		String dateT = hashMap.get(neededFields()[2]);
		boolean verified = verf(md5Packed);
		if(verified) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			
			GregorianCalendar startDate = Console.getDate(dateF);
			GregorianCalendar endDate = Console.getDate(dateT);
			for (GregorianCalendar oneDate : Console.getDateInterval(startDate, endDate)) {
				String datePacked = textPackager(sdf.format(oneDate.getTime()).toString());
				PackedQuery myPackedQuery1 = new PackedQuery("DELETE FROM availability_Calendar WHERE MD5 = " + md5Packed + " AND dateAvailable = "+ datePacked +";"
						, 1);
				PackedQuery myPackedQuery2 = new PackedQuery("DELETE FROM booking_Calendar WHERE MD5 = " + md5Packed + " AND booked_date = "+ datePacked +";"
						, 1);
				
				AcquisionFields[] aFields = {AcquisionFields.BOOKED_PRICE, AcquisionFields.BOOKER_SIN};
				ArrayList<HashMap> veryResult = 
						Acquisition.acquire(aFields, "booking_Calendar", md5Packed + " = MD5 AND (TO_DAYS(booked_date) - TO_DAYS(now()) > 0)");
				if(veryResult.size() != 0) {
					HashMap<AcquisionFields, Object> someResult = veryResult.get(0);
					CancellationRecorder.cancellationRecord(md5Packed, 
							datePacked, 
							someResult.get(AcquisionFields.BOOKED_PRICE).toString(), 
							someResult.get(AcquisionFields.BOOKER_SIN).toString(), 
							whoDid.host);
				}
				justPQ.add(myPackedQuery1);
				justPQ.add(myPackedQuery2);
			}
			JustQueryPackger jqp = new JustQueryPackger(justPQ);					
			return jqp;
		}
		else {
			Console.logln("No matching listing data, deletion denied. ");
			return null;
		}
		
	}
	
	private boolean verf(String md5) {
		AcquisionFields[] aFields = {AcquisionFields.MD5};
		return Acquisition.acquire(aFields, "listing", md5 + " = md5").size() != 0;
	}
	
	@Override
	public String[] neededFields() {
		String[] stuffs = {
				myFields.MD5.toString().toLowerCase(),
				myFields.DATEFROM.toString().toLowerCase(), 
				myFields.DATETO.toString().toLowerCase(), 
		};
		return stuffs;
	}


}
