package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

public class ShowRenters extends QueryEnum {

	public final static String mySyntax = "show renters";

	public enum myFields {
		ROOM_MD5_HAS_BOOKED, BOOK_DATE_START, BOOK_DATE_END,
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		String hostSIN = textPackager(hashMap.get(neededFields()[0]));

		Console.logln("With filters you provided that are not empty, we found results below: ");

		// For acquiring data

		ArrayList<HashMap> rslt = everyStuff(hashMap);

		// For logging
		for (HashMap<AcquisionFields, Object> hm : rslt) {
			Console.log(hm.get(AcquisionFields.MD5));
			Console.log("\t");
			Console.log(hm.get(AcquisionFields.BOOKED_DATE));
			Console.log("\t");
			Console.logln(hm.get(AcquisionFields.NAME));
		}

		// To confuse the console
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;
	}

	public ArrayList<HashMap> everyStuff(HashMap<String, String> hashMap) {
		AcquisionFields[] aFields = { AcquisionFields.MD5, AcquisionFields.BOOKED_DATE, AcquisionFields.NAME };
		if (hashMap.get(neededFields()[0]).toString().trim().equals("") && hashMap.get(neededFields()[1]).toString().trim().equals("")
				&& hashMap.get(neededFields()[2]).toString().trim().equals("")) { // just get renters
			AcquisionFields[] maFields = { AcquisionFields.NAME };
			return Acquisition.acquire(maFields, "user", "uType = 'renter'");
		} else {
			String joinedTable = "(user u RIGHT JOIN booking_Calendar b ON u.SIN = b.booker_sin)";

			 if (hashMap.get(neededFields()[1]).toString().trim().equals("")
					&& hashMap.get(neededFields()[2]).toString().trim().equals("")) { // designated
				return Acquisition.acquire(aFields, joinedTable, 
						"(uType = 'renter' AND MD5 = '" + hashMap.get(neededFields()[0]).toString() + "' )");
				
			} else if (hashMap.get(neededFields()[0]).toString().trim().equals("")
					&& hashMap.get(neededFields()[1]).toString().trim().equals("")) { // before
				return Acquisition.acquire(aFields, joinedTable, "uType = 'renter' AND TO_DAYS(booked_date) - TO_DAYS("
						+ hashMap.get(neededFields()[2]).toString() + ") < 0");
				
			} else if (hashMap.get(neededFields()[0]).toString().trim().equals("")
					&& hashMap.get(neededFields()[2]).toString().trim().equals("")) { // after
				return Acquisition.acquire(aFields, joinedTable, "uType = 'renter' AND TO_DAYS(booked_date) - TO_DAYS("
						+ hashMap.get(neededFields()[1]).toString() + ") > 0");
			
			} else if (hashMap.get(neededFields()[0]).toString().trim().equals("")) { // period
				return Acquisition.acquire(aFields, joinedTable,
						"(uType = 'renter') " + "AND"
								+ " (TO_DAYS(booked_date) - TO_DAYS(" + hashMap.get(neededFields()[2]).toString()
								+ ") < 0 AND TO_DAYS(booked_date) - TO_DAYS("
								+ hashMap.get(neededFields()[1]).toString() + ") > 0)");
			} else if (hashMap.get(neededFields()[1]).toString().trim().equals("")) { // before with designated
				return Acquisition.acquire(aFields, joinedTable,
						"(uType = 'renter' AND MD5 = '" + hashMap.get(neededFields()[0]).toString()
								+ "' ) AND TO_DAYS(booked_date) - TO_DAYS(" + hashMap.get(neededFields()[2]).toString()
								+ ") < 0");
				
			} else if (hashMap.get(neededFields()[2]).toString().trim().equals("")) {// after with designated
				return Acquisition.acquire(aFields, joinedTable,
						"(uType = 'renter' AND MD5 = '" + hashMap.get(neededFields()[0]).toString()
								+ "' ) AND TO_DAYS(booked_date) - TO_DAYS(" + hashMap.get(neededFields()[1]).toString()
								+ ") > 0",
						null);
				
			} else {// period with designated
				return Acquisition.acquire(aFields, joinedTable,
						"(uType = 'renter' AND MD5 = '" + hashMap.get(neededFields()[0]).toString() + "' ) " + "AND"
								+ " (TO_DAYS(booked_date) - TO_DAYS(" + hashMap.get(neededFields()[2]).toString()
								+ ") < 0 AND TO_DAYS(booked_date) - TO_DAYS("
								+ hashMap.get(neededFields()[1]).toString() + ") > 0)",
						null);
			}
		}

	}

	// private boolean verified(String sin) {
	// AcquisionFields[] aFields = {AcquisionFields.SIN};
	// ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "user", sin +" = sin
	// ");
	// return rslt.size() != 0;
	// }

	@Override
	public String[] neededFields() {
		String[] stuffs = { myFields.ROOM_MD5_HAS_BOOKED.toString(), myFields.BOOK_DATE_START.toString(),
				myFields.BOOK_DATE_END.toString() };
		return stuffs;
	}

}
