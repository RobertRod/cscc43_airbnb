package query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import airbnb.AcquisionFields;
import airbnb.Console;

public class ShowPotentialCommertial extends QueryEnum {

	public final static String mySyntax = "show potential commercial";

	public enum myFields {
		CITY, COUNTRY
	}
	
	@Override
	public Query execute(HashMap<String, String> hashMap) {
		// TODO Auto-generated method stub
		ArrayList<HashMap> rslt = everyStuff(hashMap);
		TreeMap<String, Integer> count = new TreeMap<String, Integer>();
		int fullcount = 0;
		for (HashMap<AcquisionFields, Object> hm : rslt) {
			String thisSin = hm.get(AcquisionFields.SIN_HOST).toString();
			if(count.containsKey(thisSin)) {
				count.replace(thisSin, count.get(thisSin) + 1);
			}
			else {
				count.put(thisSin, 1);
			}
			++fullcount;
		}
		for(String sinnn : count.descendingKeySet()) {
			if(count.get(sinnn) * 10 >= fullcount) {
				Console.logln("User with sin " + sinnn + " might be a commercial hosts.");
			}
		}
		// To confuse the console
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;
	}
	
	public ArrayList<HashMap> everyStuff(HashMap<String, String> hashMap) {
		AcquisionFields[] aFields = { AcquisionFields.CITY, AcquisionFields.SIN_HOST, AcquisionFields.COUNTRY};
		String cityName = hashMap.get(neededFields()[0]).toString().toLowerCase().replaceAll("\\s+$", "");
		String CountryName = hashMap.get(neededFields()[1]).toString().toLowerCase().replaceAll("\\s+$", "");

		if(!CountryName.trim().equals("")) {
			return Acquisition.acquire(aFields,
					"city, country, o.sin_host ",
					"own o JOIN general_location g ON o.MD5 = g.MD5 ", 
					"g.country = " + textPackager(CountryName));
		}
		else if(!cityName.equals("")) {
			return Acquisition.acquire(aFields,
					"city, country, o.sin_host ",
					"own o JOIN general_location g ON o.MD5 = g.MD5 ", 
					"g.city = " + textPackager(cityName));
		}
		else {
			return null;
		}		
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = { 
				myFields.CITY.toString(), 
				myFields.COUNTRY.toString(), 
				};
		return stuffs;
	}

}
