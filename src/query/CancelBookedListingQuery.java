package query;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;
import query.CancellationRecorder.whoDid;

public class CancelBookedListingQuery extends QueryEnum {
	public final static String mySyntax = "cancel booking";
	
	public enum myFields{
		MD5, BOOKED_DATE, SIN
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		String md5Packed = textPackager(hashMap.get(neededFields()[0]));
		String dateNotPacked = hashMap.get(neededFields()[1]);
		String inpSIN_renter = hashMap.get(neededFields()[2]);
		
		boolean verified = verf(inpSIN_renter);
		if(verified) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			GregorianCalendar date = Console.getDate(dateNotPacked);
			Console.logln("DELETE FROM booking_calendar WHERE"+
					" TO_DAYS(booked_date) = TO_DAYS(" + sdf.format(date.getTime()).toString() +
					") AND MD5 = " + md5Packed +
					" AND booker_sin = " + inpSIN_renter + ";");
			PackedQuery myPackedQuery1 = new PackedQuery(
					"DELETE FROM booking_calendar WHERE"+
					" TO_DAYS(booked_date) = TO_DAYS(" + sdf.format(date.getTime()).toString() +
					") AND MD5 = " + md5Packed +
					" AND booker_sin = " + inpSIN_renter + ";",
					3);
			
			AcquisionFields[] aFields = {AcquisionFields.BOOKED_PRICE, AcquisionFields.BOOKER_SIN};
			ArrayList<HashMap> veryResult = 
					Acquisition.acquire(aFields, "booking_Calendar", md5Packed + " = MD5 AND (TO_DAYS(booked_date) - TO_DAYS(now()) > 0)");
			if(veryResult.size() != 0) {
				HashMap<AcquisionFields, Object> someResult = veryResult.get(0);
				CancellationRecorder.cancellationRecord(md5Packed, 
						textPackager(dateNotPacked), 
						someResult.get(AcquisionFields.BOOKED_PRICE).toString(), 
						someResult.get(AcquisionFields.BOOKER_SIN).toString(), 
						whoDid.renter);
			}
			
			justPQ.add(myPackedQuery1);
			JustQueryPackger jqp = new JustQueryPackger(justPQ);					
			return jqp;
		}
		else {
			Console.logln("The renter does not exist, deletion denied. ");
			return null;
		}
		
	}
	
	
	private boolean verf(String sin) {
		AcquisionFields[] aFields1 = {AcquisionFields.SIN};
		return (Acquisition.acquire(aFields1, "user", "(sin = " + sin + ")" + " AND (uType = 'renter')").size() == 1); // &&
				//(Acquisition.acquire(aFields2, "availability_Calendar", "dateAvailable = " + "'" + md5 + "'").size() >= 1);
	}
	
	@Override
	public String[] neededFields() {
		String[] stuffs = {
				myFields.MD5.toString().toLowerCase(),
				myFields.BOOKED_DATE.toString().toLowerCase(), 
				myFields.SIN.toString().toLowerCase(), 
		};
		return stuffs;
	}
}
