package query;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

public class BookListingQueryEnum extends QueryEnum {

	public final static String mySyntax = "book listing";

	public enum myFields {
		MD5, DATE_FROM_YYYYMMDD, DATE_TO_YYYYMMDD, SIN
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();

		String roomMD5Packed = textPackager(hashMap.get(neededFields()[0]));
		String inpSIN = hashMap.get(neededFields()[3]);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		GregorianCalendar startDate = Console.getDate(hashMap.get(neededFields()[1]));
		GregorianCalendar endDate = Console.getDate(hashMap.get(neededFields()[2]));
		// String price = hashMap.get(neededFields()[3]);
		// String date = sdf.format(dateForm.getTime()).toString();

		for (GregorianCalendar oneDate : Console.getDateInterval(startDate, endDate)) {
			String dateString = textPackager(sdf.format(oneDate.getTime()).toString());
			boolean verified = verf(roomMD5Packed, dateString, inpSIN);
			if (verified) {
				AcquisionFields[] aFields = { AcquisionFields.PRICE };

				Float price = (Float) Acquisition
						.acquire(aFields, "availability_Calendar",
								roomMD5Packed + " = md5 AND dateAvailable = " + dateString)
						.get(0).get(AcquisionFields.PRICE);
				String[] stuffs = { roomMD5Packed, dateString, price.toString(), inpSIN};
				PackedQuery myPackedQuery1 = new PackedQuery("DELETE FROM availability_Calendar WHERE"
						+ " dateAvailable = " + dateString + " AND MD5 = " + roomMD5Packed + ";", 1);
				PackedQuery myPackedQuery2 = new PackedQuery(
						"INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin)" + " VALUES "
								+ bracketCommaMaker(stuffs),
						1);
				justPQ.add(myPackedQuery1);
				justPQ.add(myPackedQuery2);
			} else {
				Console.logln("No matching listing data on " + dateString + ", booking denied. ");
			}
		}
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;

	}

	private boolean verf(String md5, String date, String sin) {
		AcquisionFields[] aFields1 = { AcquisionFields.DATEAVAILABLE };
		AcquisionFields[] aFields2 = { AcquisionFields.UTYPE };
		return (Acquisition.acquire(aFields1, "availability_Calendar",
				"(dateAvailable = " + date + ") && " + "MD5 = " + md5).size() == 1)
				&& (Acquisition.acquire(aFields2, "user", "sin = " + sin + " AND uType = 'renter'").size() == 1);
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = { myFields.MD5.toString().toLowerCase(), myFields.DATE_FROM_YYYYMMDD.toString().toLowerCase(),
				myFields.DATE_TO_YYYYMMDD.toString().toLowerCase(), myFields.SIN.toString().toLowerCase() };
		return stuffs;
	}

}
