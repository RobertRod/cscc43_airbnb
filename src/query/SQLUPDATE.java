package query;

import java.util.ArrayList;
import java.util.HashMap;

public class SQLUPDATE extends QueryEnum {

	public final static String mySyntax = "SQLUPDATE";

	public enum myFields {
		COMMAND
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {		
		String command = hashMap.get(neededFields()[0]);
		// To confuse the console
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		PackedQuery myPackedQuery = new PackedQuery(command, 1);
		justPQ.add(myPackedQuery);
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;
	}


	@Override
	public String[] neededFields() {
		String[] stuffs = { 
				myFields.COMMAND.toString()
				};
		return stuffs;
	}
}
