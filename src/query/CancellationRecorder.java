package query;

import java.sql.SQLException;

import airbnb.Console;

public class CancellationRecorder {
	public static enum whoDid{
		host, renter
	}
	public static void cancellationRecord(String MD5Packed, String datePacked, String price, String sin, whoDid who) {
		String[] texts = {MD5Packed, datePacked, price, sin, QueryEnum.textPackager(who.toString())};
		try {
			Console.directDBManipulate(
					"INSERT INTO book_cancel_history (MD5, booked_date, booked_price, booker_sin, performed_by)" + 
			" VALUES " + QueryEnum.bracketCommaMaker(texts) );
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
	}
}
