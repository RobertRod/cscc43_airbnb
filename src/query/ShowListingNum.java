package query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import airbnb.AcquisionFields;
import airbnb.Console;
import query.ShowHostOwning.myFields;

public class ShowListingNum extends QueryEnum {

	public final static String mySyntax = "show listing count";

	@Override
	public Query execute(HashMap<String, String> hashMap) {		
//		String amenityPacked = textPackager(hashMap.get(neededFields()[0]));
		
		Console.logln("Please ignore the line above :D \r\n \n");
		
		ArrayList<HashMap> rslt = everyStuff();
		
		TreeMap<String, Integer> count = new TreeMap<String, Integer>();
		int fullcount = 0;
		for (HashMap<AcquisionFields, Object> hm : rslt) {
			String thisSin = hm.get(AcquisionFields.CITY).toString();
			String thatSin = hm.get(AcquisionFields.COUNTRY).toString();
			if(count.containsKey(thisSin)) {
				count.replace(thisSin, count.get(thisSin) + 1);
			}
			else {
				count.put(thisSin, 1);
			}
			if(count.containsKey(thatSin)) {
				count.replace(thatSin, count.get(thatSin) + 1);
			}
			else {
				count.put(thatSin, 1);
			}
			++fullcount;
		}
		for(String sinnn : count.keySet()) {
			Console.logln(sinnn + " has " + count.get(sinnn).toString() + " listings.");
		}
		// To confuse the console
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		JustQueryPackger jqp = new JustQueryPackger(justPQ);
		return jqp;
	}

	public ArrayList<HashMap> everyStuff() {
		// For acquiring data
		AcquisionFields[] aFields = {AcquisionFields.COUNTRY, AcquisionFields.CITY};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields,  "general_location", "1=1");
		return rslt;
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = {};
		return stuffs;
	}

}
