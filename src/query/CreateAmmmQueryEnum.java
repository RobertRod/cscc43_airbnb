package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;
import query.CreateHostQueryEnum.myFields;

public class CreateAmmmQueryEnum extends QueryEnum {

public final static String mySyntax = "new amenity";
	
	public enum myFields{
		MD5, AMENITY, SIN
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		String[] valuesToBeInserted = {
				textPackager(hashMap.get(neededFields()[0])), 
				textPackager(hashMap.get(neededFields()[1])),
		};
		boolean verified = verf(textPackager(hashMap.get(neededFields()[0])), textPackager(hashMap.get(neededFields()[2])), textPackager(hashMap.get(neededFields()[2])));
		if(verified) {
			PackedQuery myPackedQuery = new PackedQuery("INSERT INTO amenities (MD5, amen)" + 
					" VALUES " + bracketCommaMaker(valuesToBeInserted)
					, 1);
			justPQ.add(myPackedQuery);
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
			return jqp;
		}
		else {
			Console.logln("This amenity already existed or the host is not recognized, creation denied. ");
			return null;
		}
	}

	private boolean verf(String packedMD5, String packedAmen, String sin_host) {
		AcquisionFields[] aFields1 = {AcquisionFields.AMEN, AcquisionFields.MD5};
		AcquisionFields[] aFields2 = {AcquisionFields.SIN_HOST, AcquisionFields.MD5};
		return Acquisition.acquire(aFields1, "amenities", packedMD5 + " = MD5 AND amen = " + packedAmen).size() == 0 &&
				Acquisition.acquire(aFields2, "own", "(sin_host = " + sin_host + ") && " + "MD5 = " + packedMD5).size() == 1;
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = {
				myFields.MD5.toString(),
				myFields.AMENITY.toString(),
				myFields.SIN.toString(),
		};
		return stuffs;
	}

}
