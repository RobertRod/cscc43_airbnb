package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

public class DeleteUser extends QueryEnum {
	public final static String mySyntax = "delete user";
	
	public enum myFields{
		SIN
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		String sin = hashMap.get(neededFields()[0]);
		boolean verified = verf(sin);
		if(verified) {
			PackedQuery myPackedQuery = new PackedQuery("DELETE FROM user WHERE SIN = " + sin + ";"
					, 1);
			justPQ.add(myPackedQuery);
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
					
			return jqp;
		}
		else {
			Console.logln("This user still have unfinished bookings, deletion denied. Please reject all booking requests first. ");
			return null;
		}
	}

	private boolean verf(String SIN) {
		AcquisionFields[] aFields = {AcquisionFields.SIN};
		Boolean result2 = Acquisition.acquire(aFields, 
				"user u JOIN booking_Calendar b ON u.sin = b.booker_sin", 
				SIN + " = sin AND uType = 'renter' AND (TO_DAYS(booked_date) - TO_DAYS(now()) > 0)").size() == 0;
		Boolean result1 = Acquisition.acquire(aFields, 
				"(user u JOIN own o ON u.sin = o.sin_host) JOIN booking_Calendar b ON o.MD5 = b.MD5", 
				SIN + " = sin AND uType = 'host' AND (TO_DAYS(booked_date) - TO_DAYS(now()) > 0)").size() == 0;
		return result1 && result2;
	}

	@Override
	public String[] neededFields() {
		String[] stuffs = {
				myFields.SIN.toString().toLowerCase(),
		};
		return stuffs;
	}
}
