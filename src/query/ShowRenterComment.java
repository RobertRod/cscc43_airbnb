package query;

import java.util.ArrayList;
import java.util.HashMap;

import java.sql.Blob;
import java.sql.SQLException;

import airbnb.AcquisionFields;
import airbnb.Console;
import query.ShowHostOwning.myFields;

public class ShowRenterComment extends QueryEnum {

	public final static String mySyntax = "show renter comment";

	public enum myFields {
		RENTER_SIN
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {		
		String renterSIN = textPackager(hashMap.get(neededFields()[0]));
		if(verified(renterSIN)) {
			Console.logln("The comment of the renter " + renterSIN + "is:");
			
			// For acquiring data
			AcquisionFields[] aFields = {AcquisionFields.RATING, AcquisionFields.COMMENT};
			ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "comment", "commenteeSIN = " + renterSIN);
			
			//Console.logln(rslt.get(1).get(AcquisionFields.RATING));
			// For logging
			for(HashMap<AcquisionFields, Object> hm : rslt) {
				String commentB = (String) hm.get(AcquisionFields.COMMENT);
				//String comment;
				//comment = new String(commentB.getBytes(1l, (int) commentB.length()));
				Console.logln("Rate: " + hm.get(AcquisionFields.RATING));
				Console.logln("Comment: " + commentB);
			}
			
			// To confuse the console
			ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
			return jqp;
		}
		else {
			Console.logln("The renter with given sin cannot be found.");
		}
		return null;
	}

	private boolean verified(String sin) {
		AcquisionFields[] aFields = {AcquisionFields.SIN};
		ArrayList<HashMap> rslt = Acquisition.acquire(aFields, "user", sin +" = sin " + " AND (uType = 'renter')");
		return rslt.size() == 1;
	}
	
	@Override
	public String[] neededFields() {
		String[] stuffs = { 
				myFields.RENTER_SIN.toString()
				};
		return stuffs;
	}

}
