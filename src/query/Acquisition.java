/**
 * 
 */
package query;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import airbnb.AcquisionFields;
import airbnb.Console;

/**
 * @author wang_w571
 *
 */
public class Acquisition {

	private static void pushInArrayList(AcquisionFields[] toSelect, ArrayList<HashMap> everything, ResultSet myresult)
			throws SQLException {
		while(myresult.next()) {
			HashMap<AcquisionFields, Object> result = new HashMap<AcquisionFields, Object>();
			for(AcquisionFields fld : toSelect) {
				if(fld.isBlob) {
					Blob thatblob = myresult.getBlob(fld.toString());
					result.put(fld, new String(thatblob.getBytes(1, (int) thatblob.length())));
				}
				else {
					result.put(fld, myresult.getObject(fld.toString()));
				}
			}
			everything.add(result);
		}
	}
	
	public static ArrayList<HashMap> acquire(AcquisionFields[] toSelect, String table, String filter) {
		ArrayList<HashMap> everything = new ArrayList<HashMap>();
		try {
			ResultSet myresult = Console.directDBAcquire("SELECT "
					+ flatten(toSelect)
					+ "FROM "
					+ table
					+ " WHERE "
					+ filter
					+ ";");
			pushInArrayList(toSelect, everything, myresult);
//			myresult.close();
			return everything;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	public static ArrayList<HashMap> acquire(AcquisionFields[] toSelect, String actualSelect, String table, String filter) {
		ArrayList<HashMap> everything = new ArrayList<HashMap>();
		try {
			ResultSet myresult = Console.directDBAcquire("SELECT "
					+ actualSelect
					+ "FROM "
					+ table
					+ " WHERE "
					+ filter
					+ ";");
			pushInArrayList(toSelect, everything, myresult);
//			myresult.close();
			return everything;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	/**
	 * For Debug purpose
	 * @param toSelect
	 * @param table
	 * @param filter
	 * @param enableDebug put something here to enable debug. null is fine. 
	 * @return
	 */
//	public static ArrayList<HashMap> acquire(AcquisionFields[] toSelect, String table, String filter, Object enableDebug) {
//		ArrayList<HashMap> everything = new ArrayList<HashMap>();
//		try {
//			Console.logln("SELECT "
//					+ flatten(toSelect)
//					+ "FROM "
//					+ table
//					+ " WHERE "
//					+ filter
//					+ ";");
//			
//			ResultSet myresult = Console.directDBAcquire("SELECT "
//					+ flatten(toSelect)
//					+ "FROM "
//					+ table
//					+ " WHERE "
//					+ filter
//					+ ";");
//			
//			pushInArrayList(toSelect, everything, myresult);
////			myresult.close();
//			return everything;
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
//		
//	}
//	public static ArrayList<HashMap> acquire(AcquisionFields[] toSelect, String actualSelect, String table, String filter, Object enableDebug) {
//		ArrayList<HashMap> everything = new ArrayList<HashMap>();
//		try {
//			Console.logln("SELECT "
//					+ actualSelect
//					+ "FROM "
//					+ table
//					+ " WHERE "
//					+ filter
//					+ ";");
//			
//			ResultSet myresult = Console.directDBAcquire("SELECT "
//					+ actualSelect
//					+ "FROM "
//					+ table
//					+ " WHERE "
//					+ filter
//					+ ";");
//			pushInArrayList(toSelect, everything, myresult);
////			myresult.close();
//			return everything;
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return null;
//		}
//		
//	}
//	
	/**
	 * I do not want to care about exception processing
	 * @param toSelect the columns that will be selected
	 * @return A plain text of those column names with comma separated
	 */
	static String flatten(AcquisionFields[] toSelect) {
		String result = "";
		for (AcquisionFields fldName : toSelect) {
			result += fldName.toString();
			result += " ,";
		}
		return result.substring(0, result.length() - 1);
	}
}
