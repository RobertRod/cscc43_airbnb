package query;

import java.util.HashMap;
import java.util.Scanner;

import airbnb.Console;
import instruction.Instruction;
import instruction.PartialQueryFields;

public class Controller {
	protected static Controller instance = null;
	protected Scanner scanner;
	protected Instruction instruction;
	protected PartialQueryFields pqf;
	
	// Singleton controller
	private Controller(Scanner s) {
		this.scanner = s;
		this.instruction = Instruction.init();
		this.pqf = new PartialQueryFields();
	}
	
	public static Controller getInstance(Scanner s) {
		if (instance == null) {
			instance = new Controller(s);
		}
		return instance;
	}
	
	public Query matchInput(String userInp) {
		return pqf.getQuery(userInp, scanner, instruction);		
	}
	
	/**
	 * @param fields fields that will be asked
	 * @param scanner a scanner for input
	 * @return a hashmap that has fields given and answers corresponding. 
	 */
	public static HashMap<String, String> fieldsFiller(String[] fields, Scanner scanner) {
		HashMap<String, String> infos = new HashMap<>();

		for (String field : fields) {
			Console.logln("+ " + field + ": ");
			String missingInfo = scanner.nextLine();
			infos.put(field, missingInfo);
		}

		return infos;
	}
}
