package query;

import java.util.ArrayList;

/**
 * @author wang_w571
 * This is a class to package queries in order for console to process. 
 */
public class JustQueryPackger extends Query {
	private ArrayList<PackedQuery> justPackedQuery;
	
	/**
	 * @param justPQ the arraylist that fulfills queries that were to run
	 */
	public JustQueryPackger(ArrayList<PackedQuery> justPQ) {
		justPackedQuery = justPQ;
	}
	
	public ArrayList<PackedQuery> packQuery() {
		return justPackedQuery;
	}
}
