package query;

import java.util.ArrayList;
import java.util.HashMap;

import airbnb.Console;

public class CreateHostQueryEnum extends QueryEnum {
	public final static String mySyntax = "new host";
	
	public enum myFields{
		SIN, BIRTHDAY_YYYYMMDD, ADDRESS, NAME, JOB, CARD_NUMBER
	}

	@Override
	public Query execute(HashMap<String, String> hashMap) {
		ArrayList<PackedQuery> justPQ = new ArrayList<PackedQuery>();
		String[] valuesToBeInserted = {
				textPackager(hashMap.get(neededFields()[0])), 
				textPackager(hashMap.get(neededFields()[1])), 
				textPackager(hashMap.get(neededFields()[2])), 
				textPackager(hashMap.get(neededFields()[3])), 
				textPackager(hashMap.get(neededFields()[4])), 
				textPackager(hashMap.get(neededFields()[5])), 
				textPackager("host")
		};
		boolean verified = true;
		if(verified) {
			PackedQuery myPackedQuery = new PackedQuery("INSERT INTO user (sin, birthday, address, name, job, cardNum, utype)" + 
					" VALUES " + bracketCommaMaker(valuesToBeInserted)
					, 1);
			justPQ.add(myPackedQuery);
			JustQueryPackger jqp = new JustQueryPackger(justPQ);
			return jqp;
		}
		else {
			Console.logln("This host acocunt already existed, creation denied. ");
			return null;
		}
	}

//	private boolean verf(String SIN) {
//		AcquisionFields[] aFields = {AcquisionFields.SIN};
//		return Acquisition.acquire(aFields, "USER", SIN + " = sin AND (uType = 'host')").size() == 0;
//	}

	@Override
	public String[] neededFields() {
		String[] stuffs = {
				myFields.SIN.toString().toLowerCase(),
				myFields.BIRTHDAY_YYYYMMDD.toString().toLowerCase(),
				myFields.ADDRESS.toString().toLowerCase(),
				myFields.NAME.toString().toLowerCase(),
				myFields.JOB.toString().toLowerCase(),
				myFields.CARD_NUMBER.toString().toLowerCase()
		};
		return stuffs;
	}

}
