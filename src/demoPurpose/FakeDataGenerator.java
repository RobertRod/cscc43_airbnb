/**
 * 
 */
package demoPurpose;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import airbnb.Console;
import airbnb.DataBase;

/**
 * @author wang_w571
 * Make fake data for testing/demo purpose
 */
public class FakeDataGenerator {
	// run the function below to get fake data inserted into the database. 
	// to avoid mistake, comment the main. 
//	public static void main(String args[]) {
//		JustDOIT();
//	}
	public static void JustDOIT() {
		DataBase db;
	try {
		db = new DataBase("root", "0000");
	
		// HOST
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (810810810, 19190514, '114 SenPai Road, XiaBeiZe', 'TianSuo HaoEr', 'Ye Shou Xian Bei', 1145141919810, 'host')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (964127581, 19760214, '31 apple Road, Toronto', 'Siege', 'Engineer', 4515263978541265, 'host')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (315646841, 19600831, '521 West Cedar Swamp Court Penticton', 'Blue Poison', 'N/A', 4151716980260863, 'host')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (478194281, 19620129, '793 Lookout Drive Burin Peninsula', 'Exusiai', 'University professor', 5302714597791805, 'host')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (123456789, 19921003, '54 Marsh St. Roxboro', 'Chen', 'Police Officer', 4588918143679095, 'host')");
		
		
		//RENTER
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (481548314, 19520131, '925 N. Country Club Ave. Devon', 'Hoshiguma', 'Police Officer', 4103463059539476, 'renter')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (194567318, 19561130, '57 Plumb Branch St. Clarenville', '������', 'Student', 4086071221926280, 'renter')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (415283436, 19570329, '2 Maiden Circle King City', 'Grani', 'Manager', 4042810698362187, 'renter')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (752753687, 19631114, '95 Brickyard Street Coaldale', 'Liskarm', 'Safeguard', 4103755553219392, 'renter')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (378278246, 19650729, '925 Ridgewood St. Goderich', 'Silver Ash', 'CEO', 4005044505440904, 'renter')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (856438343, 19671215, '6 Wrangler St. Renfrew', 'Saria', 'N/A', 4002675998492044, 'renter')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (317585433, 19700430, '910 Lake Street Chibougamau', 'Ifrit', 'Monitor', 5148383633854858, 'renter')");
	    db.updateDatabase("INSERT INTO user (sin, birthday, address, name, job, cardNum, uType) VALUES (810810818, 19720222, '214 E. Blue Spring Street Mont-Laurier', 'Eyjafjalla', 'Translator', 5193162681292847, 'renter')");
		
	        
        Console.logln("USER DONE");
      //LISTING

    	////APART
    	db.updateDatabase("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type) VALUES "
            		+ "('B8B453D68CC1BC1F37201C664D859A21', '925 N. Country Club Ave.', 35.6655691, 139.6675234, 'Apartment')"); // ye shou ju
        db.updateDatabase("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type) VALUES "
            		+ "('D14B67D1ED9620CECFA88637147EFF80', '57 Plumb Branch St.', 43.647384, -79.387633, 'Apartment')");
        db.updateDatabase("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type) VALUES "
            		+ "('3714559D532DE760B4B225D4CB887CEE', '2 Maiden Circle', 45.396394, -75.659511, 'Apartment')");
    	
    	////HOUSE
    	db.updateDatabase("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type) VALUES "
            		+ "('10D88522505D1415416BD5EB714ED07F', '95 Brickyard Street', 45.390656, -75.666001, 'House')");
        db.updateDatabase("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type) VALUES "
            		+ "('99778E3F726A7E1E0218E12F0B3E84A6', '925 Ridgewood St.', 45.405988, -75.648122, 'House')");

    	////ROOM
    	db.updateDatabase("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type) VALUES "
            		+ "('9218B85B64C2A6E88C179F377185BDAB', '6 Wrangler St.', 43.807957, -79.199592, 'Room')");
        db.updateDatabase("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type) VALUES "
            		+ "('D4EB63B07F68A78EFD8D43F27F5631C8', '910 Lake Street', 43.793234, -79.202826, 'Room')");
        db.updateDatabase("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type) VALUES "
            		+ "('BB4D5A02F60331E78742D10146753104', '214 E. Blue Spring Street', 43.792855, -79.414727, 'Room')");
        db.updateDatabase("INSERT INTO listing (MD5, roomAddress, latitude, longitude, house_type) VALUES "
            		+ "('7BC542647A896F749B1FD0E70F2FF7E8', '9882 College St.', 43.775031, -79.409684, 'Room')");
		
	    Console.logln("LISTING DONE");
	    
	 // AMMM
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('B8B453D68CC1BC1F37201C664D859A21', 'Wifi')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('B8B453D68CC1BC1F37201C664D859A21', 'Breakfast')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('B8B453D68CC1BC1F37201C664D859A21', 'Heating')");

	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('D14B67D1ED9620CECFA88637147EFF80', 'Wifi')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('D14B67D1ED9620CECFA88637147EFF80', 'Breakfast')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('D14B67D1ED9620CECFA88637147EFF80', 'Heating')");

	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('3714559D532DE760B4B225D4CB887CEE', 'Wifi')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('3714559D532DE760B4B225D4CB887CEE', 'Breakfast')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('3714559D532DE760B4B225D4CB887CEE', 'Air conditioning')");

	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('B8B453D68CC1BC1F37201C664D859A21', 'Wifi')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('B8B453D68CC1BC1F37201C664D859A21', 'Breakfast')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('B8B453D68CC1BC1F37201C664D859A21', 'Heating')");

	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('D4EB63B07F68A78EFD8D43F27F5631C8', 'Wifi')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('D4EB63B07F68A78EFD8D43F27F5631C8', 'Breakfast')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('D4EB63B07F68A78EFD8D43F27F5631C8', 'Dryer')");

	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('10D88522505D1415416BD5EB714ED07F', 'Wifi')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('10D88522505D1415416BD5EB714ED07F', 'Breakfast')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('10D88522505D1415416BD5EB714ED07F', 'Private bathroom')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('10D88522505D1415416BD5EB714ED07F', 'Carbon monoxide detector')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('10D88522505D1415416BD5EB714ED07F', 'Self check-in')");
	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('10D88522505D1415416BD5EB714ED07F', 'Smoke detector')");

	    db.updateDatabase("INSERT INTO amenities (MD5, amen) VALUES " + "('99778E3F726A7E1E0218E12F0B3E84A6', 'Shampoo')");
	    
	    Console.logln("AMMM DONE");
		//POSITION
	    db.updateDatabase("INSERT INTO general_location (MD5, city, country) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 'Shimo-kitazawa', 'Japan')");
	    db.updateDatabase("INSERT INTO general_location (MD5, city, country) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 'Toronto', 'Canada')");
	    db.updateDatabase("INSERT INTO general_location (MD5, city, country) VALUES ('3714559D532DE760B4B225D4CB887CEE', 'Ottawa', 'Canada')");
	    db.updateDatabase("INSERT INTO general_location (MD5, city, country) VALUES ('10D88522505D1415416BD5EB714ED07F', 'Ottawa', 'Canada')");
	    db.updateDatabase("INSERT INTO general_location (MD5, city, country) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 'Ottawa', 'Canada')");
	    db.updateDatabase("INSERT INTO general_location (MD5, city, country) VALUES ('9218B85B64C2A6E88C179F377185BDAB', 'Scarborough', 'Canada')");
	    db.updateDatabase("INSERT INTO general_location (MD5, city, country) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 'Scarborough', 'Canada')");
	    db.updateDatabase("INSERT INTO general_location (MD5, city, country) VALUES ('BB4D5A02F60331E78742D10146753104', 'North York', 'Canada')");
	    db.updateDatabase("INSERT INTO general_location (MD5, city, country) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 'North York', 'Canada')");
		
	    Console.logln("POSITION DONE");
		//OWNERSHIP
	    db.updateDatabase("INSERT INTO own (sin_host, MD5) VALUES (810810810 ,'B8B453D68CC1BC1F37201C664D859A21')");
	    db.updateDatabase("INSERT INTO own (sin_host, MD5) VALUES (964127581 ,'D14B67D1ED9620CECFA88637147EFF80')");
	    db.updateDatabase("INSERT INTO own (sin_host, MD5) VALUES (964127581 ,'3714559D532DE760B4B225D4CB887CEE')");
	    db.updateDatabase("INSERT INTO own (sin_host, MD5) VALUES (964127581 ,'10D88522505D1415416BD5EB714ED07F')");
	    db.updateDatabase("INSERT INTO own (sin_host, MD5) VALUES (123456789 ,'99778E3F726A7E1E0218E12F0B3E84A6')");
	    db.updateDatabase("INSERT INTO own (sin_host, MD5) VALUES (478194281 ,'9218B85B64C2A6E88C179F377185BDAB')");
	    db.updateDatabase("INSERT INTO own (sin_host, MD5) VALUES (123456789 ,'D4EB63B07F68A78EFD8D43F27F5631C8')");
	    db.updateDatabase("INSERT INTO own (sin_host, MD5) VALUES (964127581 ,'BB4D5A02F60331E78742D10146753104')");
	    db.updateDatabase("INSERT INTO own (sin_host, MD5) VALUES (123456789 ,'7BC542647A896F749B1FD0E70F2FF7E8')");
	    Console.logln("OWNERSHIP DONE");
		
	    //IDLE
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20190810, 114)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20190811, 514)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20190812, 1919)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20190813, 810)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20190814, 863)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20190815, 114514)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20190816, 1919810)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20190817, 114)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20190818, 514)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190801, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190802, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190803, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190804, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190805, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190806, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190807, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190808, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190809, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190810, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190811, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190812, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190813, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('D14B67D1ED9620CECFA88637147EFF80', 20190814, 50)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('3714559D532DE760B4B225D4CB887CEE', 20190820, 40)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('3714559D532DE760B4B225D4CB887CEE', 20190821, 40)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('3714559D532DE760B4B225D4CB887CEE', 20190822, 40)");    
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190901, 35)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190902, 35)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190903, 35)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190904, 35)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190905, 35)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190906, 35)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190907, 35)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190908, 35)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190909, 35)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190910, 35)");    
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 20191225, 80)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 20191226, 80)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 20191227, 80)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 20191228, 80)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 20191229, 80)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 20191230, 80)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 20191231, 80)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 20200101, 80)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('99778E3F726A7E1E0218E12F0B3E84A6', 20200102, 80)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191110, 60)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191111, 60)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191112, 60)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191113, 60)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191114, 60)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191115, 60)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191116, 60)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191117, 60)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191118, 60)");
	    db.updateDatabase("INSERT INTO availability_Calendar (MD5, dateAvailable, price) VALUES ('BB4D5A02F60331E78742D10146753104', 20190815, 100)");
	    Console.logln("AVA DONE");
		
		
		
	  //OCCUPY
		db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190820, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190821, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190822, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190823, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190824, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190825, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190826, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190827, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190828, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190829, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('10D88522505D1415416BD5EB714ED07F', 20190830, 30, 810810818)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191119, 60, 194567318)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('7BC542647A896F749B1FD0E70F2FF7E8', 20191120, 60, 194567318)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190720, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190721, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190722, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190723, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190724, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190725, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190726, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190727, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190728, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190729, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190730, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190731, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190801, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190802, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190803, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190804, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190805, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190806, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190807, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190808, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190809, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190810, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190811, 200, 752753687)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('D4EB63B07F68A78EFD8D43F27F5631C8', 20190812, 200, 752753687)");    
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('9218B85B64C2A6E88C179F377185BDAB', 20190722, 50, 856438343)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('9218B85B64C2A6E88C179F377185BDAB', 20190729, 50, 856438343)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('9218B85B64C2A6E88C179F377185BDAB', 20190805, 50, 856438343)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('9218B85B64C2A6E88C179F377185BDAB', 20190812, 50, 856438343)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('9218B85B64C2A6E88C179F377185BDAB', 20190819, 50, 856438343)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('9218B85B64C2A6E88C179F377185BDAB', 20190826, 50, 856438343)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('9218B85B64C2A6E88C179F377185BDAB', 20190902, 50, 856438343)");
	    db.updateDatabase("INSERT INTO booking_Calendar (MD5, booked_date, booked_price, booker_sin) VALUES ('BB4D5A02F60331E78742D10146753104', 20190606, 20, 378278246)");

	    Console.logln("OCCU DONE");
		
		
	  //COMMENT
	    db.updateDatabase("INSERT INTO commentOnListing (renterSin, listingMD5, rating, comment) VALUES "
				+ "(378278246, 'BB4D5A02F60331E78742D10146753104', '4', 'Just if I can adjust the air conditioning...')");
	    db.updateDatabase("INSERT INTO commentOnListing (renterSin, listingMD5, rating, comment) VALUES "
				+ "(752753687, 'D4EB63B07F68A78EFD8D43F27F5631C8', '5', 'Great quality')");
	    db.updateDatabase("INSERT INTO commentOnListing (renterSin, listingMD5, rating, comment) VALUES "
				+ "(856438343, '9218B85B64C2A6E88C179F377185BDAB', '5', NULL)");            
	    
	    
	    db.updateDatabase("INSERT INTO comment (commenterSIN, commenteeSIN, rating, comment) VALUES "
				+ "(964127581, 378278246, '3', 'This guy is noisy')");
        //D14B67D1ED9620CECFA88637147EFF80
        db.updateDatabase("INSERT INTO comment (commenterSIN, commenteeSIN, rating, comment) VALUES "
                + "(123456789, 752753687, '5', '')");
        //D4EB63B07F68A78EFD8D43F27F5631C8
        db.updateDatabase("INSERT INTO comment (commenterSIN, commenteeSIN, rating, comment) VALUES "
                + "(478194281, 856438343, '0', 'Do not let this guy enter your house!')");           
        //9218B85B64C2A6E88C179F377185BDAB   
	    
	    Console.logln("COMM DONE");
	    

	    //CANCELLATION
	    db.updateDatabase("INSERT INTO book_cancel_history (MD5, booked_date, booked_price, booker_sin, performed_by) VALUES ('3714559D532DE760B4B225D4CB887CEE', 20190701, 40, 378278246, 'Renter')");
	    db.updateDatabase("INSERT INTO book_cancel_history (MD5, booked_date, booked_price, booker_sin, performed_by) VALUES ('3714559D532DE760B4B225D4CB887CEE', 20190702, 40, 378278246, 'Renter')");
	    db.updateDatabase("INSERT INTO book_cancel_history (MD5, booked_date, booked_price, booker_sin, performed_by) VALUES ('3714559D532DE760B4B225D4CB887CEE', 20190703, 40, 378278246, 'Renter')");
	    db.updateDatabase("INSERT INTO book_cancel_history (MD5, booked_date, booked_price, booker_sin, performed_by) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20200210, 80, 194567318, 'Host')");
	    db.updateDatabase("INSERT INTO book_cancel_history (MD5, booked_date, booked_price, booker_sin, performed_by) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20200211, 80, 194567318, 'Host')");
	    db.updateDatabase("INSERT INTO book_cancel_history (MD5, booked_date, booked_price, booker_sin, performed_by) VALUES ('B8B453D68CC1BC1F37201C664D859A21', 20200212, 80, 194567318, 'Host')");
	    Console.logln("CANCEL DONE");
	
	} catch (ClassNotFoundException | NoSuchAlgorithmException | SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
}
